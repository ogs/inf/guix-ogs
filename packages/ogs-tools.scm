(define-module (ogs-tools)
  #:use-module (guix)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages check)
  #:use-module (gnu packages image-processing)
  #:use-module (gnu packages geo)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages jupyter)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages simulation)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (ogs-package))

(define-public python-ogstools
  (package
    (name "python-ogstools")
    (version "0.5.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.opengeosys.org/ogs/tools/ogstools")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "047b9mkjnxcjd1x9agq7ynkfpxwxk4yw9ji2prn08k3mqbm6is8k"))))
    (build-system pyproject-build-system)
    (arguments
     (list
      #:phases #~(modify-phases %standard-phases
                   (add-after 'unpack 'remove-ogs-dep
                     (lambda _
                       ;; Remove ogs dependency
                       (substitute* "pyproject.toml"
                         (("\"ogs>=6.5.0\",\n")
                          ""))))
                   (replace 'check
                     (lambda* (#:key tests? #:allow-other-keys)
                       (setenv "OGS_USE_PATH" "1")
                       (when tests?
                         ;; meshlib requires tests/data which is not part of
                         ;; the install tree,
                         ;; test_studies_convergence.py fails when
                         ;; --with-input=ogs=ogs-petsc-ssd
                         (invoke "xvfb-run"
                                 "pytest"
                                 "--ignore=tests/test_meshlib.py"
                                 "--ignore=tests/test_studies_convergence.py"
                                 "--ignore=tests/test_logparser.py"
                                 "--ignore=tests/test_ogs6py.py"
                                 "--ignore=tests/test_shapefile_meshing.py"
                                 "--ignore=tests/test_variables.py"))))
                   ;; Disable the sanity check, which fails with the following error:
                   ;; ...checking requirements: ERROR: ogstools==0.0.0
                   ;; DistributionNotFound(Requirement.parse('vtk'), {'pyvista'})
                   ;; May want to uncomment from time to time to check for other issues.
                   (delete 'sanity-check))))
    (propagated-inputs (list gmsh
                             ogs
                             python-h5py
                             python-jupytext
                             python-matplotlib
                             python-meshio
                             python-nbconvert
                             python-typeguard
                             python-pandamesh
                             python-vtuinterface
                             python-pandas
                             python-papermill
                             python-pillow
                             python-pint
                             python-pyvista
                             python-pyyaml
                             python-rich
                             python-scipy))
    (native-inputs (list bash-minimal
                         python-pytest
                         python-pytest-dependency
                         python-parameterized
                         xvfb-run
                         python-setuptools
                         python-wheel))
    (home-page "https://ogstools.opengeosys.org")
    (synopsis
     "Collection of tools aimed at evolving into a modeling toolchain around OGS")
    (description
     "This package provides a collection of Python tools aimed at evolving into a
modeling toolchain around @code{OpenGeoSys}.")
    (license license:bsd-3)))

(define-public python-pandamesh
  (package
    (name "python-pandamesh")
    (version "0.2.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/Deltares/pandamesh")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1gnrxw8zxcdkkqzw08lrmnz946bp507r9zn1djhaxxqjs09yzmqp"))))
    (build-system pyproject-build-system)
    (arguments
     (list
      #:test-flags
      ;; tests requiring network disabled
      #~(list "--ignore=tests/test_data.py")))
    (propagated-inputs (list gmsh
                             python-geopandas
                             python-matplotlib
                             python-pooch
                             python-shapely
                             python-triangle
                             python-xarray))
    (native-inputs (list python-hatchling python-pytest))
    (home-page "https://deltares.github.io/pandamesh/")
    (synopsis "From geodataframe to mesh")
    (description "From geodataframe to mesh.")
    (license license:expat)))

(define-public python-triangle
  (package
    (name "python-triangle")
    (version "20250106")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/drufat/triangle")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1fj8a64k94mibdz5i6zzmlgs0bvsfsa5qns33ppv8wl306yxq1d3"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-numpy))
    (native-inputs (list python-pytest python-setuptools python-wheel))
    (home-page "https://github.com/drufat/triangle")
    (synopsis "Python bindings to the triangle library")
    (description
     "Triangle is a python wrapper around Jonathan Richard
Shewchuk's two-dimensional quality mesh generator and delaunay
triangulator library.")
    (license license:lgpl3)))

(define-public python-vtuinterface
  (package
    (name "python-vtuinterface")
    (version "0.704")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/joergbuchwald/VTUinterface")
             (commit "de52fc1f63399af547cd9275e8520d886bd9378c")))
       (sha256
        (base32 "11ayf9yqy0rfa22msdhxkkjh2idvw1ps9dmd3j74zbp39bxygyyz"))))
    (build-system python-build-system)
    (propagated-inputs (list python-pandas python-lxml python-scipy vtk python-meshio))
    (home-page "https://joergbuchwald.github.io/VTUinterface-doc/")
    (synopsis "Python interface for VTU / PVD files")
    (description
     "Python-Interface for reading and writing VTU files and exactracting time series data from PVD/VTU files")
    (license license:bsd-3)
    (arguments
     '(#:phases
       (modify-phases %standard-phases
         ;; Disable tests for now because they require internet access
         ;; for downloading test files.
         (delete 'check)
         ;; Disable the sanity check, which fails with the following error:
         ;;
         ;;   ...checking requirements: ERROR: VTUinterface==0.704 DistributionNotFound(Requirement.parse('vtk'), {'VTUinterface'})
         (delete 'sanity-check))))))

(define-public python-glaciationBCs
  (package
    (name "python-glaciationBCs")
    (version "1.2.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/cbsilver/glaciationBCs.git")
             (commit "v1.2.3")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0i29720mm7gskqwl8z62kyz9cs3pmbs3m0dsmlzfk8617387isbd"))))
    (inputs (list python-numpy python-matplotlib python-pandas))
    (build-system pyproject-build-system)
    (arguments
     (list
      #:tests? #f))
    (home-page "https://github.com/cbsilver/glaciationBCs")
    (synopsis
     "Glacial cycles via boundary conditions for hydro-geological simulations")
    (description
     "Python package for capturing glacial cycles via boundary conditions for hydro-geological simulations, especially with the multi-field Simulator OpenGeoSys.")
    (license license:expat)))
