(define-module (ogs-dependencies)
  #:use-module (guix)
  #:use-module (guix packages)
  #:use-module (gnu packages python)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy))

(define-public tetgen
  (package
    (name "tetgen")
    (synopsis
     "A Quality Tetrahedral Mesh Generator and a 3D Delaunay Triangulator")
    (license license:agpl3)
    (description
     "TetGen is a program to generate tetrahedral meshes of any 3D polyhedral domains. TetGen generates exact constrained Delaunay tetrahedralizations, boundary conforming Delaunay meshes, and Voronoi partitions.")
    (home-page "http://www.tetgen.org/")
    (version "1.5.1-2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ufz/tetgen")
                    (commit version)))
              (sha256
               (base32
                "0hd8mvmzybvlgvfx131p8782v8hggmg53vlkgazpsw9d6ympfcrv"))))
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f
       #:configure-flags (list "-DCMAKE_POSITION_INDEPENDENT_CODE=ON")))))

(define-public iphreeqc
  (package
    (name "iphreeqc")
    (version "3.5.0-3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/ufz/iphreeqc")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "12wiqyzpzx89k9c7q07w4ypnppvi6s88k6jjsnlnvaxfafyvrbw3"))))
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f
       #:configure-flags (list "-DCMAKE_POSITION_INDEPENDENT_CODE=ON")))
    (home-page "https://www.usgs.gov/software/phreeqc-version-3")
    (synopsis "C++ library for performing aqueous geochemical calculations")
    (description
     "PHREEQC implements several types of aqueous models including two
ion-association aqueous models.  This package contains modifications for
OpenGeoSys")
    (license license:public-domain)))
