(define-module (ogs-package)
  #:use-module (guix)
  #:use-module (guix packages)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages check)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages image-processing)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages xml)
  #:use-module (guix-science packages physics)
  #:use-module (guix-science-nonfree packages mkl)
  #:use-module (guix-science-nonfree packages maths)
  #:use-module (ogs-dependencies))

(define-public ogs
  (package
    (name "ogs-serial")
    (version "6.5.4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.opengeosys.org/ogs/ogs.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "07qrs40kxa356b32hvihaz6k8zdfh9hqnc9nf5q7qbflb1dqk4rd"))))
    (build-system cmake-build-system)
    (arguments
     `(#:build-type "Release"
       #:configure-flags (list "-GNinja" "-DOGS_USE_MFRONT=ON"
                               "-DOGS_USE_NETCDF=ON"
                               (string-append "-DMFrontGenericInterface_DIR="
                                              (assoc-ref %build-inputs "mgis")
                                              "/share/mgis/cmake"))
       #:cmake ,cmake ;for newer CMake version
       #:phases (modify-phases %standard-phases
                  (add-before 'configure 'set-env
                    (lambda _
                      (setenv "NINJAFLAGS"
                              (string-append "-k1" ;less verbose build output
                                             ;; Respect the '--cores' option of 'guix build'.
                                             " -j"
                                             (number->string (parallel-job-count))))))
                  (replace 'build
                    (lambda* (#:key parallel-build? #:allow-other-keys)
                      (apply invoke "cmake" "--build" "."
                             (if parallel-build?
                                 `("--parallel" ,(number->string (parallel-job-count)))
                                 '()))))
                  (replace 'check
                    (lambda* (#:key tests? #:allow-other-keys)
                      (when tests?
                        (invoke "cmake" "--build" "." "-t" "test"))))
                  (replace 'install
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let* ((out (assoc-ref outputs "out")))
                        (invoke "cmake" "--install" ".")
                        ;; Fix missing executable permissions
                        (chmod (string-append out
                                              "/lib/libOgsMFrontBehaviour.so")
                               #o755)))))))
    (inputs (list boost
                  eigen
                  exprtk
                  hdf5
                  iphreeqc
                  json-modern-cxx
                  libxml2
                  mgis
                  netcdf
                  netcdf-cxx4
                  pybind11-2.10
                  python
                  range-v3
                  spdlog
                  tclap
                  tetgen
                  tfel
                  zlib
                  vtk
                  xmlpatch))
    (propagated-inputs (list metis)) ;to get mpmetis into PATH
    (native-inputs (list git ninja))
    (synopsis "OpenGeoSys")
    (description
     "Simulation of thermo-hydro-mechanical-chemical (THMC) processes in porous and fractured media")
    (home-page "https://www.opengeosys.org")
    (properties '((tunable? . #t)))
    (license license:bsd-3)))

(define-public ogs-ssd
  (package
    (inherit ogs)
    (name "ogs-ssd")
    (arguments
     (substitute-keyword-arguments (package-arguments ogs)
       ((#:configure-flags flags)
        `(cons* "-DOGS_BUILD_PROCESSES=SteadyStateDiffusion"
                ,flags))))
    (synopsis "OGS with SteadyStateDiffusion only (for faster build testing)")))

(define-public ogs-mkl
  (package
    (inherit ogs)
    (name "ogs-mkl")
    (inputs (modify-inputs (package-inputs ogs)
              (prepend intel-oneapi-mkl)))
    (arguments
     (substitute-keyword-arguments (package-arguments ogs)
       ((#:configure-flags flags)
        `(cons* "-DOGS_USE_MKL=ON"
                (string-append "-DMKLROOT="
                               (assoc-ref %build-inputs "intel-oneapi-mkl"))
                ,flags))))
    (synopsis "OGS with MKL (sequential!)")))

(define-public ogs-mkl-ssd
  (package
    (inherit ogs-mkl)
    (name "ogs-mkl-ssd")
    (inputs (modify-inputs (package-inputs ogs-mkl)
              (prepend intel-oneapi-mkl)))
    (arguments
     (substitute-keyword-arguments (package-arguments ogs-mkl)
       ((#:configure-flags flags)
        `(cons* "-DOGS_BUILD_PROCESSES=SteadyStateDiffusion"
                ,flags))))
    (synopsis "OGS with SteadyStateDiffusion only and MKL (sequential!)")))

(define-public ogs-petsc
  (package
    (inherit ogs)
    (name "ogs-petsc")
    (inputs (modify-inputs (package-inputs ogs)
              (prepend openmpi petsc-openmpi)
              (replace "hdf5" hdf5-parallel-openmpi)
              (replace "netcdf-cxx4" netcdf-cxx4-parallel-openmpi)))
    (native-inputs (modify-inputs (package-native-inputs ogs)
                     (prepend pkg-config)))
    (arguments
     (substitute-keyword-arguments (package-arguments ogs)
       ((#:configure-flags flags)
        `(cons* "-DOGS_USE_PETSC=ON" "-DCMAKE_C_COMPILER=mpicc"
                "-DCMAKE_CXX_COMPILER=mpic++"
                ,flags))))
    (synopsis "OGS with PETSc")))

(define-public ogs-petsc-ssd
  (package
    (inherit ogs-petsc)
    (name "ogs-petsc-ssd")
    (arguments
     (substitute-keyword-arguments (package-arguments ogs-petsc)
       ((#:configure-flags flags)
        `(cons* "-DOGS_BUILD_PROCESSES=SteadyStateDiffusion"
                ,flags))))
    (synopsis
     "OGS with PETSc and SteadyStateDiffusion only (for faster build testing)")))

;; Build with (needs eigen-unsupported, use internal eigen in vtk,
;; vtk 9.3.0 tarfile does not contain eigen source code) e.g.:
;;
;; guix time-machine -C channels.scm -- pack -RR --format=squashfs bash coreutils ogs-petsc-mkl
;;   --with-source=eigen=https://gitlab.com/libeigen/eigen/-/archive/9000b3767770f6dd0f4cfb12f4e19c71921885a4/eigen-9000b3767770f6dd0f4cfb12f4e19c71921885a4.tar.gz
;;   --without-tests=eigen
;;   --with-configure-flag=vtk="-DVTK_MODULE_USE_EXTERNAL_VTK_eigen=OFF"
;;   --with-source=vtk=https://www.vtk.org/files/release/9.3/VTK-9.3.1.tar.gz
;;   --with-source=ogs-petsc-mkl=https://gitlab.opengeosys.org/ogs/ogs/-/archive/master/ogs-master.tar.gz
(define-public ogs-petsc-mkl
  (package
    (inherit ogs-petsc)
    (name "ogs-petsc-mkl")
    (inputs (modify-inputs (package-inputs ogs-petsc)
              (prepend intel-oneapi-mkl)
              (replace "petsc-openmpi" petsc-openmpi-mkl)))
    (arguments
     (substitute-keyword-arguments (package-arguments ogs-petsc)
       ((#:configure-flags flags)
        `(cons* "-DOGS_USE_MKL=ON"
                (string-append "-DMKLROOT="
                               (assoc-ref %build-inputs "intel-oneapi-mkl"))
                ,flags))))
    (synopsis
     "OGS with PETSc and MKL (sequential only)")))

(define-public ogs-petsc-mkl-ssd
  (package
    (inherit ogs-petsc-mkl)
    (name "ogs-petsc-mkl-ssd")
    (arguments
     (substitute-keyword-arguments (package-arguments ogs-petsc-mkl)
       ((#:configure-flags flags)
        `(cons* "-DOGS_BUILD_PROCESSES=SteadyStateDiffusion"
                ,flags))))
    (synopsis
     "OGS with PETSc and MKL (sequential only) and SteadyStateDiffusion only (for faster build testing)")))

(define-public ogs-dev
  (package
    (inherit ogs)
    (name "ogs-dev")
    (inputs (modify-inputs (package-inputs ogs)
              (delete tetgen)))
    (version "6.5.5-dev")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.opengeosys.org/ogs/ogs.git")
             (commit "6dc96385d1eae9375b78455415ae30f6d7e2d04e"))) ;rm tetgen
       (file-name (git-file-name name version))
       (sha256
        (base32 "0ck7wyhgy83zxs0djrigwbqnzf7vavnmqj4pmls8mkilqzrm1pk3"))))
    (synopsis "OGS head")))

;; return package
ogs
