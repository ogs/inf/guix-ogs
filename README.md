## Channel setup

Put this into your `~/.config/guix/channels.scm`:

```scheme
(cons (channel
        (name 'guix-ogs)
        (url "https://gitlab.opengeosys.org/ogs/inf/guix-ogs.git"))
      %default-channels)
```

Then run `guix pull`. OGS packages should be available now:

```bash
$ guix search ^ogs

name: ogs
version: 6.5.1
outputs:
+ out: everything
systems: x86_64-linux
dependencies: boost@1.80.0 eigen@3.4.0 exprtk@0.0.2 git@2.41.0 hdf5@1.10.9 iphreeqc@3.5.0-3 json-modern-cxx@3.11.2 libxml2@2.9.14 metis@5.1.0 mgis@2.1 ninja@1.11.1 pybind11@2.10.4
+ python@3.10.7 range-v3@0.12.0 spdlog@1.13.0 tclap@1.2.4-1 tetgen@1.5.1-1 tfel@4.0.1 vtk@9.2.2 xmlpatch@0.4.3 zlib@1.2.13
location: ogs-package.scm:30:2
...
```

## Use it for OGS development

```bash
guix shell -C -D
cmake --preset release
cmake --build --preset release
```

In that shell there is no network conncetion and your build-dirs will not persist. See [`guix shell` docs](https://guix.gnu.org/manual/en/html_node/Invoking-guix-shell.html) for more info.

## Install ogs into your profile

```bash
guix install ogs-petsc # Installs latest release
ogs --version

# Installs current master
guix install ogs-petsc --with-commit=ogs-petsc=5bc716e95ba232638425fcde99cff5be672ce125
```

## Create a container

```bash
# Latest ogs release, petsc config, tuned for EVE / JUWELS
guix pack -RR --format=squashfs --tune=skylake-avx512 bash coreutils ogs-petsc
# Copy created .squashfs-file to HPC environment, have fun
```
